package com.example.a18f_0196_ass2;



import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.BreakIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Signup extends AppCompatActivity {
    public static final String Extra_Email = "email";
    public static final String Extra_Password = "password";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Button signupButton = findViewById(R.id.signup);
        Button loginButton = findViewById(R.id.login);
        EditText password = findViewById(R.id.password);
        EditText firstname = findViewById(R.id.firstname);
        EditText lastname = findViewById(R.id.lastname);
        EditText email = findViewById(R.id.email);

        EditText passwordagain = findViewById(R.id.password_again);
        EditText phone = findViewById(R.id.phone);
        EditText address = findViewById(R.id.address);

        Pattern pattern;
        final String PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PATTERN);

        Intent signupIntent = new Intent(this, com.example.a18f_0196_ass2.MainActivity.class);
        Intent loginIntent = new Intent (this, com.example.a18f_0196_ass2.MainActivity.class);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = true;
                Matcher matcher = pattern.matcher(password.getText().toString());

                if (password.getText().toString().length() < 8 || !matcher.matches() ||
                        !password.getText().toString().equals(passwordagain.getText().toString()))
                {
                    isValid = false;
                }

                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (firstname.getText().toString().isEmpty() || lastname.getText().toString().isEmpty()
                        ||  email.getText().toString().isEmpty() || phone.getText().toString().isEmpty()
                        || !email.getText().toString().matches(emailPattern)                )
                {
                    isValid = false;
                }


                    loginIntent.putExtra("email", email.getText().toString());
                    loginIntent.putExtra("password", password.getText().toString());
                    startActivity(loginIntent);
                    finish();


            }
        });

        loginButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                startActivity(loginIntent);
                finish();
            }
        });
    }
}