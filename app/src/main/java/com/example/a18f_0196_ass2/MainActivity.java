package com.example.a18f_0196_ass2;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a18f_0196_ass2.DialogueBox;
import com.example.a18f_0196_ass2.HomeScreen;
import com.example.a18f_0196_ass2.Signup;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String reqname = getIntent().getStringExtra(Signup.Extra_Email);
        String reqpass = getIntent().getStringExtra(Signup.Extra_Password);

        EditText username = findViewById(R.id.username);
        EditText password = findViewById(R.id.password);

        Button loginButton = findViewById(R.id.login);
        Button signupButton = findViewById(R.id.signup);

        Intent homeIntent = new Intent(this, HomeScreen.class);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (username.getText().toString().equals(reqname) && password.getText().toString().equals(reqpass) ) {
                    Toast.makeText(MainActivity.this, "Successfully Logged In ! Hurray..", Toast.LENGTH_SHORT).show();
                    startActivity(homeIntent);
                    finish();
                }
                else {
                    DialogueBox Dialog = new DialogueBox();
                    Dialog.show(getSupportFragmentManager(), "Example");
                }
            }
        });

        Intent signupIntent = new Intent(this, Signup.class);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(signupIntent);
                finish();
            }
        });
    }

}